const http = require("http");
const fs = require("fs");
const fspromise = require("fs").promises;
const url = require("url");
const { URL } = require('url');
const path = require("path");
const pathToStorage = "./storage/";
let logs_;
try {
  logs_ = JSON.parse(fs.readFileSync("logs.json"));
} catch {
  logs_ = {
    logs: [],
  };
}

module.exports = () => {
  http
    .createServer(async function (req, res) {
      const urlObj = new URL(`http://host${req.url}`);
      if (req.method == "GET") {
        if (urlObj.pathname.startsWith("/file")) {
          const filename = path.basename(urlObj.pathname);
          

          try {
            const result = await findAndReadFile(filename);
            try {
              await writeLog("Reading file with name '" + filename + "'");
            } catch (e) {
              console.log(e);
            }
            res.writeHead(200, { "Content-Type": "text/html" });
            res.end(result);
          } catch (e) {
            try {
              await writeLog(
                "Reading file with name '" + filename + "' unsuccess"
              );
            } catch (e) {
              console.log(e);
            }
            console.log(e);
            res.writeHead(400, { "Content-Type": "text/html" });
            res.end("Can't find the file " + filename);
          }
        } else if (urlObj.pathname.startsWith("/logs")) {
          try {
            await writeLog("Reading logs");
          } catch (e) {
            console.log(e);
          }
          const from_ = urlObj.searchParams.get("from") || null;
          const to_ = urlObj.searchParams.get("to") || null;
          res.writeHead(200, { "Content-Type": "aplication/json" });
          const firstLogTime =  logs_.logs[0].time;
          const lastLogTime = logs_.logs[logs_.logs.length-1].time;
          if((!from_ || from_<=firstLogTime) && (!to_ || to_>=lastLogTime)){
            res.end(JSON.stringify(logs_.logs));
          }else if(from_>=firstLogTime && to_>=lastLogTime){
            res.end(JSON.stringify(logs_.logs.filter(el=> el.time>=from_)))
          }else if(from_<=firstLogTime && to_<=lastLogTime){
            res.end(JSON.stringify(logs_.logs.filter(el=> el.time<=to_)))
          }else{
            res.end(JSON.stringify(logs_.logs.filter(el=> el.time<=to_ && el.time>=from_)))
          }
        } else {
          res.writeHead(400, { "Content-Type": "text/html" });
          res.end("Can't find resourse");
        }
      } else {
        if (urlObj.pathname.startsWith("/file")) {
          const filename = path.basename(urlObj.pathname);
          console.log(filename);
          
          let data = urlObj.searchParams.get('content');
          if (!data) {
            data = "";
          }
          try {
            const doesExist = await checkFileExists(filename);
            let message = "New file with name '" + filename + "' saved";
            if (doesExist) {
              message = "File with name '" + filename + "' wrote";
            }
            await writeToFileOrCreate(filename, data);
            try {
              await writeLog(message);
            } catch (e) {
              console.log(e);
            }
            res.writeHead(200, { "Content-Type": "text/html" });
            res.end(message);
          } catch (e) {
            res.writeHead(400, { "Content-Type": "text/html" });
            console.log(e);
            try {
              writeLog("Write to file '" + filename + "' unseccess");
            } catch (e) {
              console.log(e);
            }
            res.end("Write to file '" + filename + "' unseccess");
          }
        } else {
          res.writeHead(400, { "Content-Type": "text/html" });
          res.end("Can't find resourse");
        }
      }
    })
    .listen(8080);
};

async function writeLog(text) {
  // console.log(logs_.logs);
  logs_.logs.push({
    massage: text,
    time: Date.now(),
  });

  return fspromise.writeFile("logs.json", JSON.stringify(logs_));
}

async function findAndReadFile(filename) {
  return fspromise.readFile(pathToStorage+filename, "utf8");
}
async function writeToFileOrCreate(filename, data) {
  return fspromise.writeFile(pathToStorage+filename, data);
}

function checkFileExists(filename) {
  return fs.promises
    .access(pathToStorage+filename, fs.constants.F_OK)
    .then(() => true)
    .catch(() => false);
}





